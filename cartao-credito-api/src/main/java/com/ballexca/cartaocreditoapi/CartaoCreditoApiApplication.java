package com.ballexca.cartaocreditoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartaoCreditoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaoCreditoApiApplication.class, args);
	}

}
